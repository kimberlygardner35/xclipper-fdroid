package com.kpstv.xclipper.ui.fragments.settings

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kpstv.xclipper.App
import com.kpstv.xclipper.App.AUTO_SYNC_PREF
import com.kpstv.xclipper.App.BIND_DELETE_PREF
import com.kpstv.xclipper.App.BIND_PREF
import com.kpstv.xclipper.App.CONNECT_PREF
import com.kpstv.xclipper.App.FORCE_REMOVE_PREF
import com.kpstv.xclipper.App.HELP_PREF
import com.kpstv.xclipper.App.LOGOUT_PREF
import com.kpstv.xclipper.App.UID
import com.kpstv.xclipper.R
import com.kpstv.xclipper.data.provider.DBConnectionProvider
import com.kpstv.xclipper.data.provider.PreferenceProvider
import com.kpstv.xclipper.extensions.listeners.ResponseListener
import com.kpstv.xclipper.extensions.utils.Utils
import com.kpstv.xclipper.extensions.utils.Utils.Companion.logoutFromDatabase
import com.kpstv.xclipper.extensions.utils.Utils.Companion.showConnectionDialog
import com.kpstv.xclipper.ui.fragments.AnimatePreferenceFragment
import com.kpstv.xclipper.ui.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import es.dmoral.toasty.Toasty
import javax.inject.Inject

@AndroidEntryPoint
class AccountPreference : PreferenceFragmentCompat() {

    @Inject
    lateinit var preferenceProvider: PreferenceProvider

    @Inject
    lateinit var dbConnectionProvider: DBConnectionProvider

    private val mainViewModel: MainViewModel by viewModels()

    private var autoSyncPreference: SwitchPreferenceCompat? = null
    private var bindPreference: SwitchPreferenceCompat? = null
    private var bindDeletePreference: SwitchPreferenceCompat? = null
    private var logPreference: Preference? = null
    private var connectPreference: Preference? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.account_pref, rootKey)

        logPreference = findPreference(LOGOUT_PREF)
        logPreference?.setOnPreferenceClickListener {
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Connect Preference */
        connectPreference = findPreference(CONNECT_PREF)
        connectPreference?.setOnPreferenceClickListener {
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Database binding preference */
        bindPreference = findPreference(BIND_PREF)
        bindPreference?.setOnPreferenceChangeListener { _, newValue ->
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Auto sync preference */
        autoSyncPreference = findPreference(AUTO_SYNC_PREF)
        autoSyncPreference?.setOnPreferenceChangeListener { _, newValue ->
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Bind delete preference */
        bindDeletePreference = findPreference(BIND_DELETE_PREF)
        bindDeletePreference?.setOnPreferenceChangeListener { _, newValue ->
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Force logout preference */
        findPreference<Preference>(FORCE_REMOVE_PREF)?.setOnPreferenceClickListener {
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }

        /** Help Preference */
        findPreference<Preference>(HELP_PREF)?.setOnPreferenceClickListener {
            Utils.showThatThisIsADemoApp(requireContext())
            true
        }
    }
}