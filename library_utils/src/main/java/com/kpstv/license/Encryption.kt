package com.kpstv.license

object Encryption {
    private var phrase = "123456"

    fun setPassword(password: String) {
        phrase = password
    }

    fun getPassword() : String = phrase

    fun String.Decrypt(): String = this

    fun String.DecryptPref(): String = this

    fun String.Encrypt(): String = this

    fun String.EncryptPref(): String = this
}